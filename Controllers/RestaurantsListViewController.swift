//
//  RestaurantsListViewController.swift
//  Vixinity
//
//  Created by Serbin Taras on 04.10.18.
//  Copyright © 2018 Sasha. All rights reserved.
//

import UIKit

final class RestaurantsListViewController: UIViewController, TableViewGenerable {
    typealias Item = RestaurantMerchant
    typealias Cell = RestaurantTableViewCell
    
    @IBOutlet private weak var tableView: UITableView!
    
    private var restaurantMerchants = [RestaurantMerchant]()
    private let networkService = RestaurantsNetworkService()
    
    var tableViewGenerator: TableViewGenerator<RestaurantsListViewController>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        tableView.register(RestaurantTableViewCell.self)
        tableViewGenerator = TableViewGenerator<RestaurantsListViewController>.init(tableView: tableView, dataSource: restaurantMerchants, responder: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchAllRestaurantMerchants()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        parent?.navigationController?.navigationBar.topItem?.title =  AppLanguageServise.retrieveValueFrom(key: "restaurants").uppercased()
    }
    
    func didSelect(item: RestaurantMerchant, index: Int) {
        let restaurantDetailViewController: RestaurantDetailViewController = storyboard!.instantiateViewController()
        restaurantDetailViewController.restaurantMerchantId = item.id
        navigationController?.pushViewController(restaurantDetailViewController, animated: true)
    }
}

private extension RestaurantsListViewController {
    func fetchAllRestaurantMerchants() {
        
        networkService.fetchAllRestaurantMerchants() { [weak self] (restaurantMerchantResponse, error) in
            self?.showCustomAlert(isNeedToShow: error != nil, image: #imageLiteral(resourceName: "error"), text: error?.localizedDescription, isAutomaticalyDissmesed: true, completion: nil)
            
            if let restaurantMerchants = restaurantMerchantResponse {
                if restaurantMerchants.hasError  {
                    if let errorCode = restaurantMerchants.errors?.first?.code {
                        let errorMessage = AppLanguageServise.retrieveValueFrom(key: "error\(errorCode)")
                        self?.showCustomAlert(image: #imageLiteral(resourceName: "error"), text: errorMessage, completion: nil)
                    }
                } else {
                    if let restaurantMerchants = restaurantMerchantResponse?.restaurantMerchants {
                          self?.restaurantMerchants = restaurantMerchants
                          self?.tableViewGenerator.reloadData(dataSource: restaurantMerchants)
                    }
                }
            }
        }
    }
}

