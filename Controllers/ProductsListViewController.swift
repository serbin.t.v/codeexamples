//
//  ProductsListViewController.swift
//  Vixinity
//
//  Created by Serbin Taras on 05.10.18.
//  Copyright © 2018 Sasha. All rights reserved.
//

import UIKit

final class ProductsListViewController: UIViewController, ProductOrderable {
  
    @IBOutlet private weak var tableView: UITableView!
    
    var products = [Product]()
    var category: ProductCategory!
    var restaurantMerchantId: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(ProductDetailTableViewCell.self)
        addRightButtonToNavigationItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addBageToNavigationItem(productsNumber:  OrderedProcuts.getNumberOfProducts(key: restaurantMerchantId))
        navigationController?.navigationBar.addCustomBackButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBar.topItem?.title = category.name.uppercased()
    }
      
    func rightNavigationButtonTapped() {
        let basketProductViewController: BasketProductViewController = storyboard!.instantiateViewController()
        basketProductViewController.restaurantMerchantId = restaurantMerchantId
        navigationController?.pushViewController(basketProductViewController, animated: true)
    }
}

extension ProductsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let orderProductViewController: OrderProductViewController = storyboard!.instantiateViewController()
        orderProductViewController.product = products[indexPath.row]
        orderProductViewController.restaurantMerchantId = restaurantMerchantId
        navigationController?.pushViewController(orderProductViewController, animated: true)
    }
}

extension ProductsListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ProductDetailTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.product = products[indexPath.row]
        
        cell.addButtonTapped = { [weak self] product in
            let orderedProduct = OrderedProcut(product: product, amount: 1, variant: product.productVariants?.first, ingredients: product.ingredients)
            OrderedProcuts.addProduct(product: orderedProduct, key: (self?.restaurantMerchantId)!)
            self?.addBageToNavigationItem(productsNumber:  OrderedProcuts.getNumberOfProducts(key: (self?.restaurantMerchantId)!))
            let text = AppLanguageServise.retrieveValueFrom(key: "itemaddedtobasket")
            self?.showCustomAlert(image: #imageLiteral(resourceName: "alertOk"), text: text, completion: nil)
        }
        
        return cell
    }
}
