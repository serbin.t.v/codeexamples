//
//  BasketProductViewController.swift
//  Vixinity
//
//  Created by Serbin Taras on 04.10.18.
//  Copyright © 2018 Sasha. All rights reserved.
//

import UIKit

final class BasketProductViewController: UIViewController, ProductOrderable {
  
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var checkOutButton: UIButton!
    @IBOutlet private weak var basketIsEmptyLabel: UILabel!
    
    var restaurantMerchantId: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showProductEmptyLabelIfNeeded()
        tableView.register(BasketTableViewCell.self)
        addRightButtonToNavigationItem()
        addBageToNavigationItem(productsNumber:  OrderedProcuts.getNumberOfProducts(key: restaurantMerchantId))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.addCustomBackButton()
        addBageToNavigationItem(productsNumber:  OrderedProcuts.getNumberOfProducts(key: restaurantMerchantId))
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        checkOutButton.addGradientToView(colors: UIColor.customGradientBlue)
    }
    
    func rightNavigationButtonTapped() { }
}

extension BasketProductViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  OrderedProcuts.getNumberOfProducts(key: restaurantMerchantId)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: BasketTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.product =  OrderedProcuts.getProducts(key: restaurantMerchantId)![indexPath.row]
        cell.deleteButtonDidTapped = { [weak self] product in
            self?.proccessDeleteProduct(product: product)
        }
        return cell
    }
}

private extension BasketProductViewController {
    
    @IBAction func checkOutButtonTapped(_ sender: UIButton) {
        let checkOutViewController: CheckOutViewController = storyboard!.instantiateViewController()
        checkOutViewController.modalPresentationStyle = .overCurrentContext
        checkOutViewController.restaurantMerchantId = restaurantMerchantId
        navigationController?.present(checkOutViewController, animated: true, completion: nil)
    }
    
    func showProductEmptyLabelIfNeeded() {
        let isHidden = OrderedProcuts.getNumberOfProducts(key: restaurantMerchantId) != 0 ? true : false
        basketIsEmptyLabel.isHidden = isHidden
    }
    
    func proccessDeleteProduct(product: OrderedProcut) {
        OrderedProcuts.deleteProduct(key: restaurantMerchantId, product: product)
        addBageToNavigationItem(productsNumber:  OrderedProcuts.getNumberOfProducts(key: restaurantMerchantId))
        tableView.reloadData()
        showProductEmptyLabelIfNeeded()
    }
}


