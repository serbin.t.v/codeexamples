//
//  CheckOutViewController.swift
//  Vixinity
//
//  Created by Serbin Taras on 2/7/19.
//  Copyright © 2019 Sasha. All rights reserved.
//

import UIKit

class CheckOutViewController: UIViewController {

    //MARK: - Properties
    private let maxHeight = UIScreen.main.bounds.height * 0.75
    private let topPostitonOfTableView:CGFloat = 58
    private let spaceToLblPrice: CGFloat = 8
    private let spaceToBtnCancel:CGFloat = 16
    //MARK: - Outlets
    @IBOutlet private weak var btnClose: UIButton!
    @IBOutlet private weak var btnCancel: UIButton!
    @IBOutlet private weak var btnPayment: UIButton!
    @IBOutlet private weak var lblTotal: UILabel!
    @IBOutlet private weak var tblProducts: UITableView!
    @IBOutlet weak var cnstViewHeight: NSLayoutConstraint!
    
    //MARK: - Model properties
    var restaurantMerchantId: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnClose.addTarget(self, action: #selector(close), for: .touchUpInside)
        btnCancel.addTarget(self, action: #selector(close), for: .touchUpInside)
        tblProducts.register(CheckOutCell.self)
        tblProducts.dataSource = self
        cnstViewHeight.constant = maxHeight
        lblTotal.text = "\(totalOrder()):-"
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        let extraSpace = lblTotal.frame.height + spaceToBtnCancel + btnCancel.frame.height
        if tblProducts.contentSize.height + topPostitonOfTableView + extraSpace < maxHeight {
            cnstViewHeight.constant = tblProducts.contentSize.height + topPostitonOfTableView + extraSpace
        }
    }

    func totalOrder() -> Int {
        var total = 0
        for orderedProduct in OrderedProcuts.getProducts(key: restaurantMerchantId)! {
            let price: Int!
            if let productVarint = orderedProduct.variant {
                price = productVarint.price * orderedProduct.amount
            } else {
                price = orderedProduct.product.price * orderedProduct.amount
            }
            total += price
        }
        return total
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CheckOutViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  OrderedProcuts.getNumberOfProducts(key: restaurantMerchantId)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CheckOutCell = tableView.dequeueReusableCell(for: indexPath)
        cell.product =  OrderedProcuts.getProducts(key: restaurantMerchantId)![indexPath.row]
        
        return cell
    }
}

extension CheckOutViewController {
    @objc func close() {
        dismiss(animated: true, completion: nil)
    }
}
