//
//  OrderProductViewController.swift
//  Vixinity
//
//  Created by Serbin Taras on 05.10.18.
//  Copyright © 2018 Sasha. All rights reserved.
//

import UIKit

final class OrderProductViewController: UIViewController, ProductOrderable {
    
    @IBOutlet private weak var addButton: UIButton!
    @IBOutlet private weak var productLogoImageView: UIImageView!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var productAmountLabel: UILabel!
    @IBOutlet private weak var ingredientsLabel: UILabel!
    @IBOutlet private weak var attributesCollectionView: UICollectionView!
    @IBOutlet private weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var editButton: UIButton!
    @IBOutlet private weak var ingredientsTitleLabel: UILabel!
    @IBOutlet private weak var ingredientsSeparatorLabel: UILabel!
    
    private var productCount = 1
    private var attributeCollections = [AttributeCollection]()
    private var selectedVariant: Variant?
    private var selectedIngredients = [String]()
    private var productPrice: Int!
    
    var product: Product!
    var restaurantMerchantId: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
        configureAttributesDataSource()
        addRightButtonToNavigationItem()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addBageToNavigationItem(productsNumber:  OrderedProcuts.getNumberOfProducts(key: restaurantMerchantId))
        navigationController?.navigationBar.addCustomBackButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBar.topItem?.title = product.name?.uppercased()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addButton.addGradient(colors: UIColor.customGradientBlue)
        collectionViewHeightConstraint.constant = attributesCollectionView.collectionViewLayout.collectionViewContentSize.height
        attributesCollectionView.reloadData()
        attributesCollectionView.layoutIfNeeded()
    }
    
    func rightNavigationButtonTapped() {
        let basketProductViewController: BasketProductViewController = storyboard!.instantiateViewController()
        basketProductViewController.restaurantMerchantId = restaurantMerchantId
        navigationController?.pushViewController(basketProductViewController, animated: true)
    }
}

extension OrderProductViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "headerView", for: indexPath) as! AttributeHeaderView
        headerView.title = product.productAttributesCollection?.productAttributes?[indexPath.section].name
        
        return headerView
    }
}

extension OrderProductViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return attributeCollections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return attributeCollections[section].attributes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: AttributeCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
        let attribute = attributeCollections[indexPath.section].attributes[indexPath.row]
        cell.attribute = attribute
        cell.didSelectItem = {  [weak self]  in
            self?.attributeCollections[indexPath.section].attributes.forEach {$0.isSelected = false }
            self?.attributeCollections[indexPath.section].attributes[indexPath.row].isSelected = true
            self?.selectAttributeVariant()
        }
        
        return cell
    }
}

private extension  OrderProductViewController {
    
    @IBAction func editButtonTapped(_ sender: UIButton) {
        
        var filterList = [Filter]()
        product.ingredients?.forEach { ingredientName in
            let isSelected = selectedIngredients.contains(where: { (name) -> Bool in
                ingredientName == name
            })
            let filter = Filter(name: ingredientName, isSelected: isSelected)
            filterList.append(filter)
        }
        let title = AppLanguageServise.retrieveValueFrom(key: "ingredients")
        showFilterList(filterSetting: filterList, titleName: title) { [weak self] filterNames in
            self?.selectedIngredients = filterNames
            self?.updateIngredientsLabel()
        }
    }
    
    @IBAction func plusButtonTapped(_ sender: UIButton) {
        productCount += 1
        updatePriceLabel(amount: productCount)
    }
    
    @IBAction func minusButtonTapped(_ sender: UIButton) {
        productCount -= 1
        if productCount < 1 {
            productCount = 1
        }
        updatePriceLabel(amount: productCount)
    }
    
    @IBAction func addButtonTapped(_ sender: UIButton) {
        let orderedProduct = OrderedProcut(product: product, amount: productCount, variant: selectedVariant, ingredients: selectedIngredients)
        OrderedProcuts.addProduct(product: orderedProduct, key: restaurantMerchantId)
        addBageToNavigationItem(productsNumber:  OrderedProcuts.getNumberOfProducts(key: restaurantMerchantId))
        let text = AppLanguageServise.retrieveValueFrom(key: "itemaddedtobasket")
        showCustomAlert(image: #imageLiteral(resourceName: "alertOk"), text: text, completion: nil)
    }
    
    func updatePriceLabel(amount: Int) {
        productAmountLabel.text = String(amount)
        let price = productPrice * amount
        priceLabel.text = String(price) + ":-"
    }
    
    func configureAttributesDataSource() {
        
        if let variant = product.productVariants {
            if !variant.isEmpty {
                product.productAttributesCollection?.productAttributes?.forEach({ (productAttribute) in
                    var attributes = [Attribute]()
                    productAttribute.values?.forEach({ (value) in
                        
                        let variant = product.productVariants?.first
                        selectedVariant = variant
                        var isSelected = false
                        variant?.attributes?.forEach({ (attribute) in
                            if attribute.name == productAttribute.name && attribute.value == value {
                                isSelected = true
                            }
                        })
                        let attribute = Attribute(name: value, isSelected: isSelected)
                        attributes.append(attribute)
                    })
                    
                    let attributeCollection = AttributeCollection(name: productAttribute.name, attributes: attributes)
                    attributeCollections.append(attributeCollection)
                })
            }
        }
    }
    
    func configureCollectionView() {
        attributesCollectionView.register(AttributeHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "headerView")
        attributesCollectionView.register(AttributeCollectionViewCell.self)
        
        if let flowLayout = attributesCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.headerReferenceSize = CGSize(width: self.attributesCollectionView.frame.width, height: 50)
            flowLayout.estimatedItemSize = CGSize(width: 1, height: 1)
        }
        
        let productType =  ProudctType(rawValue: product.productType)
        
        if  productType == .simple  {
            collectionViewHeightConstraint.constant = 0
        }
    }
    
    func selectAttributeVariant() {
        
        let selectedAttributes = attributeCollections.filter {  $0.attributes.contains(where: { (attribute) -> Bool in
            attribute.isSelected == true
        }) }
        
        var selectedVariants = product.productVariants
        
        selectedAttributes.forEach { (attributeCollection) in
            let selectedAttribute = attributeCollection.attributes.filter({ (attribute) -> Bool in
                attribute.isSelected == true
            }).first
            
            selectedVariants = selectedVariants?.filter({ (variant) -> Bool in
                (variant.attributes?.contains(where: { (variantAttribute) -> Bool in
                    variantAttribute.name == attributeCollection.name && variantAttribute.value == selectedAttribute?.name
                }))!
            })
        }
        
        selectedVariant = selectedVariants?.first
        productPrice = selectedVariant?.price
        updatePriceLabel(amount: productCount)
    }

    func configureUI() {
        editButton.isHidden = !product.allowIngredientExclude
        if let logoUrl = product.imageURL {
            productLogoImageView.imageFromServerURL(urlString: logoUrl)
        }
        descriptionLabel.text = product.description
        productPrice = product.price
        priceLabel.text = "\(product.price):-"
        selectedIngredients = product.ingredients ?? [String]()
        updateIngredientsLabel()
        if selectedIngredients.isEmpty {
            hideIngredientAttribtes()
        }
    }
    
    func hideIngredientAttribtes() {
        editButton.isHidden = true
        ingredientsTitleLabel.isHidden = true
        ingredientsSeparatorLabel.isHidden = true
    }
    
    func updateIngredientsLabel() {
        let ingrediens = selectedIngredients.reduce("", { $0 + ", " + $1 })
        ingredientsLabel.text = String(ingrediens.dropFirst())
    }
}
