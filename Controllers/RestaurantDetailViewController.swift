//
//  RestaurantDetailViewController.swift
//  Vixinity
//
//  Created by Serbin Taras on 04.10.18.
//  Copyright © 2018 Sasha. All rights reserved.
//

import UIKit

enum ProudctType: Int {
    case simple = 0
    case full
}

final class RestaurantDetailViewController: UIViewController, TableViewGenerable, ProductOrderable {
    typealias Item = ProductCategory
    typealias Cell = ProductTableViewCell
    
    @IBOutlet private weak var restaurantImageView: UIImageView!
    @IBOutlet private weak var emailLabel: UILabel!
    @IBOutlet private weak var phoneLabel: UILabel!
    @IBOutlet private weak var addressLabel: UILabel!
    @IBOutlet private weak var tableView: UITableView!
    
    private var productCategories = [ProductCategory]()
    private var restaurantMerchant: RestaurantMerchant!
    private let networkService = RestaurantsNetworkService()
    
    var restaurantMerchantId: String!
    var tableViewGenerator: TableViewGenerator<RestaurantDetailViewController>!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(ProductTableViewCell.self)
        tableViewGenerator = TableViewGenerator<RestaurantDetailViewController>(tableView: tableView, dataSource: productCategories, responder: self)
        addRightButtonToNavigationItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addBageToNavigationItem(productsNumber:  OrderedProcuts.getNumberOfProducts(key: restaurantMerchantId))
        navigationController?.navigationBar.addCustomBackButton()
        fetchRestaurantMerchant()
    }
    
    func didSelect(item: ProductCategory, index: Int) {
        let productsListViewController: ProductsListViewController = storyboard!.instantiateViewController()
        if let productIds = item.productIDs,
          let products = restaurantMerchant.productCatalog?.products {
            
            let productForCategory = products.filter { (product) -> Bool in
                productIds.contains(product.id!)
            }
            productsListViewController.products = productForCategory
            productsListViewController.category = item
            productsListViewController.restaurantMerchantId = restaurantMerchantId
        }
        navigationController?.pushViewController(productsListViewController, animated: true)
    }
    
    func rightNavigationButtonTapped() {
        let basketProductViewController: BasketProductViewController = storyboard!.instantiateViewController()
        basketProductViewController.restaurantMerchantId = restaurantMerchantId
        navigationController?.pushViewController(basketProductViewController, animated: true)
    }
}

private extension RestaurantDetailViewController {
    func fetchRestaurantMerchant() {
        
        let parameters = GetMerchantParamters(id: restaurantMerchantId)
        
        networkService.fetchSpecificMerchant(parameters: parameters) { [weak self] (restaurantMerchantResponse, error) in
            self?.showCustomAlert(isNeedToShow: error != nil, image: #imageLiteral(resourceName: "error"), text: error?.localizedDescription, isAutomaticalyDissmesed: true, completion: nil)
            
            if let restaurantMerchants = restaurantMerchantResponse {
                if restaurantMerchants.hasError  {
                    if let errorCode = restaurantMerchants.errors?.first?.code {
                        let errorMessage = AppLanguageServise.retrieveValueFrom(key: "error\(errorCode)")
                        self?.showCustomAlert(image: #imageLiteral(resourceName: "error"), text: errorMessage, completion: nil)
                    }
                } else {
                    if let restaurantMerchants = restaurantMerchantResponse?.restaurantMerchants,
                        let productCategories = restaurantMerchants.productCatalog?.productCategories {
                        self?.restaurantMerchant = restaurantMerchants
                        self?.productCategories = productCategories
                        self?.tableViewGenerator.reloadData(dataSource: productCategories)
                        self?.configureUI()
                    }
                }
            }
        }
    }
    
    func configureUI() {
        navigationController?.navigationBar.topItem?.title = restaurantMerchant.name?.uppercased()
        if let logoUrl = restaurantMerchant.logoURL {
            restaurantImageView.imageFromServerURL(urlString: logoUrl)
        }
        emailLabel.text = restaurantMerchant.email
        phoneLabel.text = restaurantMerchant.phone
        addressLabel.text = restaurantMerchant.country
        if let productCategories = restaurantMerchant.productCatalog?.productCategories {
            self.productCategories = productCategories
            tableViewGenerator.reloadData(dataSource: productCategories)
        }
    }
}

