//
//  GetMerchantParameters .swift
//  Vixinity
//
//  Created by Serbin Taras on 12.10.18.
//  Copyright © 2018 Sasha. All rights reserved.
//

import Foundation

struct GetMerchantParamters: ApiParametersProtocol {
    func convertToDictionary() -> [String : Any] {
        return ["id": id]
    }
    
    var id: String
}

