//
//  RestaurantMerchants.swift
//  Vixinity
//
//  Created by Serbin Taras on 12.10.18.
//  Copyright © 2018 Sasha. All rights reserved.
//

import Foundation


struct RestaurantMerchantsResponse: Decodable {
    let hasError: Bool
    let errors: [RequestError]?
    let restaurantMerchants: [RestaurantMerchant]?
    
    enum CodingKeys: String, CodingKey {
        case hasError = "HasError"
        case errors = "Errors"
        case restaurantMerchants = "Data"
    }
}


struct RestaurantMerchantResponse: Decodable {
    let hasError: Bool
    let errors: [RequestError]?
    let restaurantMerchants: RestaurantMerchant?
    
    enum CodingKeys: String, CodingKey {
        case hasError = "HasError"
        case errors = "Errors"
        case restaurantMerchants = "Data"
    }
}

struct RestaurantMerchant: Decodable, TableViewDataSource {
    let id: String?
    let name: String?
    let email: String?
    let country: String?
    let phone: String?
    let cityID: String?
    let description: String?
    let logoURL: String?
    let logoSmallURL: String?
    let logoSmallWideURL: String?
    let productCatalog: ProductCatalog?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "Name"
        case email = "Email"
        case country = "Country"
        case phone = "Phone"
        case cityID = "CityID"
        case description = "Description"
        case logoURL = "LogoURL"
        case logoSmallURL = "LogoSmallURL"
        case logoSmallWideURL = "LogoSmallWideURL"
        case productCatalog = "Catalog"
    }
}

struct ProductCatalog: Decodable {
    let id: String?
    let merchantID: String?
    let products: [Product]?
    let productCategories: [ProductCategory]?
    let informationNotes: [InformationNote]?
    let vATPercentage: Double?
    let productCatalogType: Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case merchantID = "MerchantID"
        case products = "AllProducts"
        case productCategories = "ProductCategories"
        case informationNotes = "InformationNotes"
        case vATPercentage = "VATPercentage"
        case productCatalogType = "ProductCatalogType"
    }
}

struct ProductCategory: Decodable, TableViewDataSource {
    let id: String
    let name: String
    let description: String?
    let imageURL: String?
    let isHidden: Bool
    let isPublished: Bool
    let availableDays: [Int]?
    let hasTimedAvailability: Bool
    let availableFromTime: String?
    let availableToTime: String?
    let notAvailableFromTime: String?
    let notAvailableToTime: String?
    let productIDs: [String]?
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name = "Name"
        case description = "Description"
        case imageURL = "ImageURL"
        case isHidden = "IsHidden"
        case isPublished = "Published"
        case availableDays = "AvailableDays"
        case hasTimedAvailability = "HasTimedAvailability"
        case availableFromTime = "AvailableFromTime"
        case availableToTime = "AvailableToTime"
        case notAvailableFromTime = "NotAvailableFromTime"
        case notAvailableToTime = "NotAvailableToTime"
        case productIDs = "ProductIDs"
    }
}

struct Product: Decodable, TableViewDataSource {
    let id: String?
    let name: String?
    let systemName: String?
    let productType: Int
    let price: Int
    let description: String?
    let imageURL: String?
    let smallImageURL: String
    let additionalImages: [String]?
    let allowIngredientExclude: Bool
    let ingredients: [String]?
    let productAttributes: [String]?
    let productVariants: [Variant]?
    let productAttributesCollection: ProductAttributesCollection?
    let includedProducts: [String]?
    let relatedProducts: [String]?
    let informationNotes: [String]?
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name = "Name"
        case systemName = "SystemName"
        case productType = "ProductType"
        case price = "Price"
        case description = "Description"
        case imageURL = "ImageURL"
        case smallImageURL = "SmallImageURL"
        case additionalImages = "AdditionalImages"
        case allowIngredientExclude = "AllowIngredientExclude"
        case ingredients = "Ingredients"
        case productAttributes = "ProductAttributes"
        case productVariants = "Variants"
        case productAttributesCollection = "ProductAttributesCollection"
        case includedProducts = "IncludedProducts"
        case relatedProducts = "RelatedProducts"
        case informationNotes = "InformationNotes"
    }

}


struct ProductAttributesCollection: Decodable {
    let productAttributes: [ProductAttribute]?
    
    enum CodingKeys: String, CodingKey {
        case productAttributes = "ProductAttributes"
    }

}

struct ProductAttribute: Decodable {
    let name: String
    let values: [String]?
    
    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case values = "Values"
    }
}

struct InformationNote: Decodable {
    var id: String?
    var name: String?
    var description: String?
    var detailsURL: String?
    var iconURL: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "Name"
        case description = "Description"
        case detailsURL = "DetailsURL"
        case iconURL = "IconURL"
    }
}

struct Variant: Decodable {
    
    let id: String
    let name: String
    let price: Int
    let attributes: [VariantAttribute]?
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name = "VariantName"
        case price = "Price"
        case attributes = "Attributes"
    }
}

struct VariantAttribute: Decodable, TableViewDataSource {
    
    let name: String
    let value: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case value = "Value"
    }
}
