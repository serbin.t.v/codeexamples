//
//  AttributeCollection.swift
//  Vixinity
//
//  Created by Serbin Taras on 16.10.18.
//  Copyright © 2018 Sasha. All rights reserved.
//

import Foundation

final class AttributeCollection {
    var name: String
    var attributes: [Attribute]
    var selectedAttribute: Attribute?
    
    init(name: String, attributes: [Attribute]) {
        self.name = name
        self.attributes = attributes
    }
}

final class Attribute {
    var name: String
    var isSelected: Bool
    
    init(name: String, isSelected: Bool) {
        self.name = name
        self.isSelected = isSelected
    }
}
