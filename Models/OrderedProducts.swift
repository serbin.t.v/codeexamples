//
//  OrderedProducts.swift
//  Vixinity
//
//  Created by Serbin Taras on 05.10.18.
//  Copyright © 2018 Sasha. All rights reserved.
//

import Foundation

struct OrderedProcuts {
    
    static var productInBasket = Dictionary<String, [OrderedProcut]>()
    
    static func getNumberOfProducts(key: String) -> Int {
        return productInBasket[key]?.count ?? 0
    }
    
    static func addProduct(product: OrderedProcut,key: String) {
        if productInBasket[key] != nil {
            productInBasket[key]?.append(product)
        } else {
            productInBasket[key] = [product]
        }
    }
    
    static func getProducts(key: String) -> [OrderedProcut]? {
        return  productInBasket[key]
    }
    
    static func deleteProduct(key: String, product: OrderedProcut) {
        if  let index = productInBasket[key]?.index(where: { (product) -> Bool in
            product.product.id == product.product.id
        }) {
            productInBasket[key]?.remove(at: index)
        }
    }
}

struct OrderedProcut {
    let product: Product
    let amount: Int
    let variant: Variant?
    let ingredients: [String]?
}
