//
//  OrderedProduct.swift
//  Vixinity
//
//  Created by Serbin Taras on 12.10.18.
//  Copyright © 2018 Sasha. All rights reserved.
//

import Foundation

final class OrderedProduct {
    var product: Product?
    var numberOfProduct = 1
    
    init(product: Product) {
        self.product = product
    }
}
