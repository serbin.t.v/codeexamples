//
//  ProductOrderable.swift
//  Vixinity
//
//  Created by Serbin Taras on 05.10.18.
//  Copyright © 2018 Sasha. All rights reserved.
//

import UIKit

@objc protocol ProductOrderable where Self: UIViewController {
    @objc func rightNavigationButtonTapped()
}

extension ProductOrderable {
    func addRightButtonToNavigationItem() {
        let customView = UIView()
        let imageView = UIImageView()
        let button = UIButton()
        customView.backgroundColor = .clear
        imageView.backgroundColor = .clear
        button.backgroundColor = .clear
        customView.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
        imageView.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        button.frame = customView.frame
        imageView.contentMode = .scaleAspectFit
        customView.addSubview(imageView)
        customView.addSubview(button)
        button.addTarget(self, action: #selector(self.rightNavigationButtonTapped), for: .touchUpInside)
        imageView.image = #imageLiteral(resourceName: "cart")
        let item = UIBarButtonItem(customView: customView)
        navigationItem.setRightBarButton(item, animated: true)
    }
    
    func  addBageToNavigationItem(productsNumber: Int) {
        
        guard productsNumber != 0 else {
            navigationItem.rightBarButtonItem?.customView?.subviews.forEach { view in
                if view.tag == 1 {
                    view.removeFromSuperview()
                }
            }
            return }
        
        if  let customView = navigationItem.rightBarButtonItem?.customView {
            let badgeView = UIView()
            badgeView.tag = 1
            badgeView.isUserInteractionEnabled = false
            let label = UILabel()
            label.isUserInteractionEnabled = false
            label.text = String(productsNumber)
            label.font = UIFont(name: "OpenSans-Regular.ttf", size: 13)
            label.adjustsFontSizeToFitWidth = true
            label.minimumScaleFactor = 0.5
            label.textAlignment = .center
            badgeView.frame = CGRect(x: customView.frame.width / 2.6, y: 0, width: customView.frame.width / 1.65, height: customView.frame.height / 1.65)
            label.frame = badgeView.bounds
            badgeView.layer.cornerRadius = badgeView.frame.height / 2
            badgeView.clipsToBounds = true
            badgeView.addSubview(label)
            badgeView.addGradientToView(colors: UIColor.customGradientBlue)
            customView.addSubview(badgeView)
        }
    }
}
