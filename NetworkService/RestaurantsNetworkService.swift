//
//  RestaurantsNetworkService.swift
//  Vixinity
//
//  Created by Serbin Taras on 12.10.18.
//  Copyright © 2018 Sasha. All rights reserved.
//

import Foundation


struct RestaurantsNetworkService: APIClientProtocol  {
    
    func fetchAllRestaurantMerchants(completion: @escaping (RestaurantMerchantsResponse?, Error?) -> ()) {
        let request = RequestGenerator<RestaurantMerchantsResponse>(type: APIRequestType.get, encodingType: EncodingType.url, endPoint: EndPoint.getAllRestaurantMerchants, parameters: nil)
        fetch(request: request) { restaurantMerchants, error  in
            completion(restaurantMerchants, error)
        }
    }
    
    func fetchSpecificMerchant(parameters: ApiParametersProtocol, completion: @escaping (RestaurantMerchantResponse?, Error?) -> ()) {
        let request = RequestGenerator<RestaurantMerchantResponse>(type: APIRequestType.get, encodingType: EncodingType.url, endPoint: EndPoint.getSpecificMerchant, parameters: parameters)
        fetch(request: request) { bookableResourceScheduleResponse, error  in
            completion(bookableResourceScheduleResponse, error)
        }
    }
}
