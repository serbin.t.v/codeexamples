//
//  AttributeHeaderView.swift
//  Vixinity
//
//  Created by Alex on 15.10.18.
//  Copyright © 2018 Sasha. All rights reserved.
//

import UIKit

final class AttributeHeaderView: UICollectionReusableView {

    private var label = UILabel()
    
    var title: String! {
        didSet {
            configureTitleLabe()
            label.text = title + ":"
        }
    }
}

private extension AttributeHeaderView {
   func configureTitleLabe() {
     label.frame = bounds
     label.textColor = UIColor.attributesGray
     addSubview(label)
    }
}
