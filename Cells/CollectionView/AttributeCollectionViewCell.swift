//
//  AttributeCollectionViewCell.swift
//  Vixinity
//
//  Created by Alex on 12.10.18.
//  Copyright © 2018 Sasha. All rights reserved.
//

import UIKit

enum AttributeCheckboxState {
    case selected
    case unselected
    
    var image: UIImage? {
        return self == .selected ? UIImage(named: "radioOn") : UIImage(named: "radioOff")
    }
    
    mutating func toggle() {
        if self == .selected {
            self = AttributeCheckboxState.unselected
        } else {
            self = AttributeCheckboxState.selected
        }
    }
}

final class AttributeCollectionViewCell: UICollectionViewCell , NibLoadableView {
    
    @IBOutlet private weak var attributeNameLabel: UILabel!
    @IBOutlet private weak var checkboxImageView: UIImageView!
    
    var didSelectItem: (() -> ())?
    
    private var attributeCheckboxState: AttributeCheckboxState = .unselected
    
    var attribute: Attribute! {
        didSet {
            attributeCheckboxState = attribute.isSelected ? .selected : .unselected
            checkboxImageView.image = attributeCheckboxState.image
            attributeNameLabel.text = attribute.name
        }
    }
}

private extension AttributeCollectionViewCell {
    
    @IBAction func checkboxButtonTapped(_ sender: UIButton) {
        didSelectItem?()
    }
}
