//
//  RestaurantTableViewCell.swift
//  Vixinity
//
//  Created by Serbin Taras on 04.10.18.
//  Copyright © 2018 Sasha. All rights reserved.
//

import UIKit


final class RestaurantTableViewCell: UITableViewCell, ConfigurableCell {
    typealias T = RestaurantMerchant
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func configure(_ item: RestaurantMerchant, at indexPath: IndexPath) {
        
        if let iconUrl = item.logoURL {
            iconImageView.imageFromServerURL(urlString: iconUrl)
        }
        nameLabel.text = item.name
        descriptionLabel.text = item.description
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        iconImageView.image = nil
    }
    
}
