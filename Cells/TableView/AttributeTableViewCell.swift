//
//  AttributeTableViewCell.swift
//  Vixinity
//
//  Created by Serbin Taras on 12.10.18.
//  Copyright © 2018 Sasha. All rights reserved.
//

import UIKit

final class AttributeTableViewCell: UITableViewCell, ConfigurableCell {
    typealias T = VariantAttribute
    
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var valueLabel: UILabel!
    
    func configure(_ item: VariantAttribute, at indexPath: IndexPath) {
        nameLabel.text = item.name + ":"
        valueLabel.text = item.value
    }
}

