//
//  ProductTableViewCell.swift
//  Vixinity
//
//  Created by Serbin Taras on 04.10.18.
//  Copyright © 2018 Sasha. All rights reserved.
//

import UIKit


final class ProductTableViewCell: UITableViewCell, ConfigurableCell {
    typealias T = ProductCategory

    @IBOutlet private weak var productNameLabel: UILabel!
    @IBOutlet private weak var productImageView: UIImageView!
    
    func configure(_ item: ProductCategory, at indexPath: IndexPath) {
        if let productLogoUrl = item.imageURL {
            productImageView.imageFromServerURL(urlString: productLogoUrl)
        }
        productNameLabel.text = item.name
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        productImageView.image = nil
    }
}
