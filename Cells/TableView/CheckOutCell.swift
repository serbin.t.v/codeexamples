//
//  CheckOutCell.swift
//  Vixinity
//
//  Created by Serbin Taras on 2/7/19.
//  Copyright © 2019 Sasha. All rights reserved.
//

import UIKit

final class CheckOutCell: UITableViewCell, NibLoadableView, TableViewGenerable {
    
    typealias Item = VariantAttribute
    typealias Cell = AttributeTableViewCell
    
    @IBOutlet private weak var productNameLabel: UILabel!
    @IBOutlet private weak var productPriceLabel: UILabel!
    
    @IBOutlet private weak var numberLabel: UILabel!
    @IBOutlet private weak var attributesTableView: UITableView!
    @IBOutlet private weak var attributesTableViewHeight: NSLayoutConstraint!
    @IBOutlet private weak var menuText: UILabel!
    
   
    var tableViewGenerator: TableViewGenerator<CheckOutCell>!
    
    var product: OrderedProcut! {
        didSet {
            attributesTableView.register(AttributeTableViewCell.self)
            tableViewGenerator = TableViewGenerator<CheckOutCell>(tableView: attributesTableView, dataSource: product.variant?.attributes ?? [VariantAttribute](), responder: self)
            numberLabel.text = String(product.amount)
            attributesTableViewHeight.constant = CGFloat((product.variant?.attributes?.count ?? 0) * 30)
            attributesTableView.layoutIfNeeded()
            productNameLabel.text = product.product.name
            let price: Int!
            if let productVarint = product.variant {
                price = productVarint.price * product.amount
            } else {
                price = product.product.price * product.amount
            }
            
            productPriceLabel.text = String(price) + ":-"
            
            let removedIngredients = product.product.ingredients?.filter({ (ingredient) -> Bool in
                return !(product.ingredients?.contains(ingredient))!
            })
            
            if let ingredients = removedIngredients {
                if !ingredients.isEmpty {
                    let ingrediens = ingredients.reduce("", { $0 + ", " + $1 })
                    menuText.text = "-\(ingrediens.dropFirst().dropFirst())"
                }
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        menuText.text = nil
    }
    
    func didSelect(item: VariantAttribute, index: Int) {
        
    }
}

