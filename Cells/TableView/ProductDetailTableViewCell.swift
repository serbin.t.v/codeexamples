//
//  ProductDetailTableViewCell.swift
//  Vixinity
//
//  Created by Serbin Taras on 05.10.18.
//  Copyright © 2018 Sasha. All rights reserved.
//

import UIKit

final class ProductDetailTableViewCell: UITableViewCell, NibLoadableView {

    @IBOutlet private weak var productNameLabel: UILabel!
    @IBOutlet private weak var productImageView: UIImageView!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var addButton: UIButton!
    @IBOutlet private weak var priceLabel: UILabel!
    
    var addButtonTapped: ((Product) -> ())?
    
    var product: Product! {
        didSet {
            productNameLabel.text = product.name
            priceLabel.text = "\(product.price):-"
            descriptionLabel.text = product.description
            
            if let imageUrl = product.imageURL {
                productImageView.imageFromServerURL(urlString: imageUrl)
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        productImageView.image = nil
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        addButton.addGradientToView(colors: UIColor.customGradientBlue)
    }
}

private extension ProductDetailTableViewCell {
    @IBAction func addButtonTapped(_ sender: UIButton) {
        addButtonTapped?(product)
    }
}
